# Project
Systematische Provenienzforschung am Germanischen Nationalmuseum
## Place
Nuremberg
## Website
[https://provenienz.gnm.de/](https://provenienz.gnm.de/)
## Duration
2014–2018
## Funding
Deutschen Zentrum Kulturgutverluste
## Fields
Provenance, Museum objects
## Description
From 2014 to 2018, the Germanisches Nationalmuseum examined its acquisitions from 1933 to 1945 in a research project focusing on the collection areas of painting up to 1800 and stained glass, sculpture up to 1800, arts and crafts up to 1800, and arts and crafts from the 19th to 21st centuries. The project was funded by the German Center for the Loss of Cultural Property for a period of three years.
The online database contains an entry for each of the approximately 1,300 objects examined, recording the respective provenance in tabular form and explaining it in a short text. Selected art dealers, other institutions and private individuals from whom the museum acquired objects are presented in short articles.
The database is flanked by an exhibition catalog and an open access publication with accompanying index volume, which expand on individual project results.