# Project
Dass Gerechtigkeit und Friede sich küssen – Repräsentationen des Friedens im vormodernen Europa.
## Place
Nuremberg
## Website
[https://friedensbilder.gnm.de/](https://friedensbilder.gnm.de/)
## Duration
2015-2018
## Funding
Leibniz Association under the funding line "National and International Networking
## Fields
Art History, Peace Representations, Coins, Sermons, Poems, Music
## Description
Within the framework of a transdisciplinary research project, the various forms of peace representations in the early modern period are being investigated. The Nuremberg subproject is dedicated to the extensive holdings of graphic sheets and medals in the Germanisches Nationalmuseum.
In the period from the 16th to the 18th century, more than 2,000 intra- and interstate peace treaties were concluded in Europe. States of war were accompanied by peace processes, which are manifested in the quantity of pre-modern peace treaties. Various representations of peace in literature, art and music took over the media dissemination. By using a common canon of motifs, a "vocabulary" of peace developed from which artists, writers, and composers drew. This vocabulary will be examined in the research project by means of examples of different cultural objects. The Leibniz Institute for European History in Mainz is examining peace sermons, the genre roots of which lie in the environment of the Augsburg Religious Peace. The project unit at the Herzog August Library in Wolfenbüttel is also dedicated to linguistic representations with the processing of German and Latin peace poems. The German Historical Institute in Rome focuses on musical representations of peace scenarios. The work results of all institutions are made accessible through the virtual research environment WissKI. The interdisciplinary processing of the source material will thus be made accessible to the research community with the help of methods of the semantic web approach and to the public after completion of the project.