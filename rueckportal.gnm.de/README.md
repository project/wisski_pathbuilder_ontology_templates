# Project
Musikinstrumente sammeln – das Beispiel Rück
## Place
Nuremberg
## Website
[https://rueckportal.gnm.de/](https://rueckportal.gnm.de/)
## Duration
2015–2018
## Funding
German Research Foundation
## Fields
Music Instruments, Graphic Art, Paintings, Photographics, Books, Documents, Letters
## Description
Sold to the Germanisches Nationalmuseum in 1962, the music history collection of Dr. Dr. h.c. Ulrich Rück is considered one of the largest private collections of musical instruments in Germany. It includes around 1,500 musical instruments and accessories, 100 sheets of graphic art, paintings, photographic documentation, a specialist library with around 500 titles, and the collection correspondence with over 17,000 documents and letters.