# Project
Die deutsche Tafelmalerei des Spätmittelalters im Germanischen Nationalmuseum. Kunsthistorische und kunsttechnologische Erforschung der Gemälde im Germanischen Nationalmuseum
## Place
Nuremberg
## Website
[https://tafelmalerei.gnm.de/](https://tafelmalerei.gnm.de/)
## Duration
2013–2022
## Funding
Leibniz Association, the Sponsors' Association of the Germanic National Museum, the German Research Foundation, and the Federal Ministry of Education and Research.
## Fields
Art History, Paintings
## Description
The Germanic National Museum preserves about 250 paintings from the 13th to 15th centuries from the entire German-speaking area. The main focus is on paintings from Nuremberg and Franconia, followed by works from Cologne, northwestern Germany and the areas around the Middle and Upper Rhine, as well as Bavaria, Austria and Swabia. Due to its scope and high artistic quality, the collection is one of the most important of its kind in the world.

Since 2013, the works have been the subject of interdisciplinary art-historical and art-technological research in successive projects. The investigations cover painting technique and the history of the paintings' creation as well as the changes to the original appearance caused by aging and reworking. Furthermore, the artistic characteristics, iconography, and representational traditions of the works are reviewed. On this basis, the previous art-historical classification of each painting, its dating, regional classification and attribution to an artist or workshop can be critically examined and corrected. The clarification of the acquisition history, origin and original location provides information about the former intended purpose of the works, which are now torn from their functional context. In an overarching perspective, new insights are also gained into the role of images as carriers of meaning, religious practice in the late Middle Ages, and the production conditions in the workshops of the time.